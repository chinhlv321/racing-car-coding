package RaceAI.AI;

import java.awt.Point;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import RaceAI.RaceClient.Car;
import RaceAI.RaceClient.Race;

public class MainAI {
	Race race;
	Vector<Car> All_cars;
	Car Mycar;
	
	public String key = "0000"; // Go-Back-Left-Right (Up - Down - Left - Right)
	
	public MainAI(Race race, Vector<Car> cars, Car Mycar){
		this.race = race;
		this.Mycar = Mycar;
		this.All_cars = cars;
	}
	
	Point last,now,next,next2;
	Random rand = new Random();
	int[] ix = {0, 1, 0, -1};
	int[] iy = {1, 0, -1, 0};
	
	Map<String,Integer> dataMovePath =new HashMap<String,Integer>();
	
	//last position
	double lx=0,ly=0;
	// last speed
	double speed = 0;
	// your AI
	boolean isTurn=false; 
	public void AI(){	
		//Block Index
		int x = (int) (this.Mycar.getx() / this.race.BlockSize());
		int y = (int) (this.Mycar.gety() / this.race.BlockSize());
		
		double speed_now = Math.sqrt((this.Mycar.getx()-lx)*(this.Mycar.getx()-lx)+(this.Mycar.gety()-ly)*(this.Mycar.gety()-ly));
		speed = (speed*2+speed_now)/3;
		lx=this.Mycar.getx();
		ly=this.Mycar.gety();
		
		if (speed>this.race.BlockSize()*0.0125) {
			this.key = "0000"; //stop
			return;
		}
		/*else if (speed>this.race.BlockSize()*0.02) {
				this.key = "0100"; //back
				return;
			}
		*/		
		this.now = new Point(x,y);
		if (this.next==null) {
			for (int i=1;i<4;i++) {
				if( this.race.BlockKind(x+ix[i], y+iy[i]) !='1')
					this.next=new Point(x+ix[i], y+iy[i]);
			}
		}
		if (this.last==null) this.last = this.now;
		/*if(isTurn) {
			this.key="0100";
			System.out.println(this.DistanceToPoint(this.now, this.last));
			if(this.DistanceToPoint(this.now, this.last)<this.race.BlockSize()*0.01f)
				this.isTurn=false;
			return;
		}*/
		
		//Car's Direction
		double v_x = Math.cos(this.Mycar.getalpha() * Math.PI/180);
		double v_y = Math.sin(this.Mycar.getalpha() * Math.PI/180);
		
		//Next Block Center Coordinate
		double block_center_x= (this.next.x + 0.5) * this.race.BlockSize();
		double block_center_y= (this.next.y + 0.5) * this.race.BlockSize();
		
		//Vector to Next Block Center from Car's position
		double c_x = block_center_x - this.Mycar.getx();
		double c_y = block_center_y - this.Mycar.gety();
		double distance2center = Math.sqrt(c_x*c_x+c_y*c_y);
		if (distance2center!=0) {
			//vector normalization
			c_x/=distance2center;
			c_y/=distance2center;
		}		
		
		if (distance2center<this.race.BlockSize()*0.5){			
			this.key = "0000"; //stop

			Point tempPoint=new Point(x+ix[0], y+iy[0]);
			int minMovePath=0;
			int i,tempI=0,countPathCanMove=0;
			for (i=0;i<4;i++) {
				if ((last.x!=x+ix[i] || last.y!=y+iy[i]) &&this.race.BlockKind(x+ix[i], y+iy[i]) !='1') {
					tempPoint=new Point(x+ix[i], y+iy[i]);
					minMovePath=this.GetDataMovePath(this.now, tempPoint);
					tempI=i;
					countPathCanMove++;
					break;
				}
			}
			for (i=i+1;i<4;i++)
				if ((last.x!=x+ix[i] || last.y!=y+iy[i]) &&this.race.BlockKind(x+ix[i], y+iy[i]) !='1'){
					countPathCanMove++;
					tempPoint=new Point(x+ix[i], y+iy[i]);
					if(this.GetDataMovePath(this.now, tempPoint)<minMovePath) {
						tempI=i;
					}
				}
			
			if(countPathCanMove==0)
				this.next=this.last;
			else
				this.next = new Point(x+ix[tempI], y+iy[tempI]);		
			if(countPathCanMove>1) {
				this.IncreaseDataMovePath(this.now, this.last);
				if(this.GetDataMovePath(this.now,this.next)==this.GetDataMovePath(this.now,this.last))
					this.next=this.last;	
				this.IncreaseDataMovePath(this.now, this.next);
				}
			this.last = this.now;	

		}
		else {
			// Go to next block center
			double inner = v_x*c_x + v_y*c_y;
			double outer = v_x*c_y - v_y*c_x;
			if (inner > 0.995){
					this.key = "1000"; //go
			} else {
				if (inner < 0){
					this.key = "0001"; //turn right
				}
				else {
					if (this.race.BlockKind(x, y)!='3')
						if (outer > 0) this.key = "0001"; //turn right
						else this.key = "0010"; //turn left
					else 
						if (outer > 0) this.key = "0010"; //turn right
						else this.key = "0001"; //turn left
				}
				}
		}
	}
	
	public int GetDataMovePath(Point a,Point b) {
		if(dataMovePath.get(a.toString()+b.toString())==null) {
			dataMovePath.put(a.toString()+b.toString(),0);
			dataMovePath.put(b.toString()+a.toString(),0);
			return 0;
		}
		else {
			return dataMovePath.get(a.toString()+b.toString());
		}		
	}
	public void IncreaseDataMovePath(Point a,Point b) {
		dataMovePath.put(a.toString()+b.toString(),GetDataMovePath(a,b)+1);
		dataMovePath.put(b.toString()+a.toString(),GetDataMovePath(b,a)+1);		
	}
	
	public double DistanceToPoint(Point a , Point b) {
		return Math.sqrt(a.x*b.x+a.y*b.y);
	}
}
